# Aufgabe 1
## Finden Sie alle Fehler!

Klonen Sie zuerst das Projekt https://stbaeumer@bitbucket.org/stbaeumer/n27gw12-klausur1-fehler-finden.git vom Bitbucket.

**Testschreiber** beschränken sich auf Syntaxfehler und Rechtschreibfehler auf der GUI. Syntaxfehler sind diejenigen Fehler, die den Nodemon bei der Bedienung des Programms abstürzen lassen.  
Konkret ist das Wort *Impressum* mehrfach falsch geschrieben worden, was auch einen Fehler verursacht, wenn Sie zur *Impressum*-Seite navigieren wollen. Das müssen Sie korrigieren!

**Klausurschreiber** müssen **zusätzlich** diejenigen logischen Fehler finden, die das Programm zwar nicht abstürzen lassen, aber ein Fehlverhalten des Programms verursachen. Konkret finden Sie diese Fehler auf der Seite **Profil bearbeiten**. 

#### Es werden ausschließlich Ihre handschriftlichen Auswertungen auf diesem Zettel eingesammelt und bewertet!

Dokumentieren Sie hier unter Angabe der fehlerhaften Zeile in der entsprechenden Datei, welche Korrektur Sie vornehmen müssen, um den Fehler zu korrigieren: Schreiben sie das wie folgt: 
_server.js, Zeile 3, Es fehlt die schließende geschweifte Klammer. Die muss ergänzt werden._
