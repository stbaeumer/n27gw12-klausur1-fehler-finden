Test und Klausur:

*Tipp:* Wenn es sich um einen bestimmten Ausdruck handelt, dann kann der mit Strg+F geuscht werden.

1. impresum.ejs umbenennen -> impressum.ejs
2. impressum.ejs, Zeile 15: Impresssum -> Impressum
3. impressum.ejs, Zeile 19: Impresssum -> Impressum
4. impressum.ejs, Zeile 23: Impresssum -> Impressum
5. index.ejs, Zeile 23: Impresssum -> Impressum

6. server.js, Zeile 106: impresssum -> impressum
7. server.js, Zeile 112: impresssum.ejs -> impressum.ejs 


Nur Klausur:

profilBearbeiten.ejs 31:
1. Aus get muss post werden. (2P)
2. Adresse und Telefonnummer werden vertauscht eingelesen. (2P)
3. Das Kennwort kann nicht geändert werden. (2P)

